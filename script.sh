#!/usr/bin/bash

# Проверка и получение изменений
git diff --name-status "$CI_COMMIT_BEFORE_SHA" "$CI_COMMIT_SHA" > /tmp/changed_files.txt

# Клонирование репозитория
git clone "https://$TARGET_ACCESS_USER:$TARGET_ACCESS_TOKEN@$TARGET_REPO_URL" target_repo
cd target_repo

# Настройка Git
git config user.email "GitlabBot@mail.com"
git config user.name "GitlabBot"

# Переключение на ветку
if ! git checkout "$CI_COMMIT_BRANCH"; then
    git checkout -b "$CI_COMMIT_BRANCH"
fi

# Применение изменений из файла
while IFS= read -r line; do
    status=$(echo "$line" | awk '{print $1}')
    file=$(echo "$line" | awk '{print $2}')
    case "$status" in
        "A"|"M")
            cp "$CI_PROJECT_DIR/$file" "$file"
            ;;
        "D")
            rm -f "$file"
            ;;
        "R100")
            old_file=$(echo "$line" | awk '{print $3}')
            mv "$old_file" "$file"
            ;;
    esac
done < /tmp/changed_files.txt

# Добавление и коммит изменений
git add .
git commit -m "Update from GitLab 1"
git push "https://$TARGET_ACCESS_USER:$TARGET_ACCESS_USER_TOKEN@$TARGET_REPO_URL" "$CI_COMMIT_BRANCH"
